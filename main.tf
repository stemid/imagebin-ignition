resource "random_string" "imagebin" {
  length = 16
  upper = false
  special = false
}

resource "aws_s3_bucket" "imagebin" {
  bucket = "imagebin-${random_string.imagebin.id}"
}

data "ignition_directory" "config" {
  path = "${var.home}/.config/imagebin"
  mode = 488
  uid = var.uid
  gid = var.gid
}

resource "random_string" "flask" {
  length = 32
}

data "ignition_file" "environment" {
  path = "${var.home}/.config/imagebin/environment"
  content {
    content = templatefile("${path.module}/templates/environment", {
      aws_access_key_id = var.aws_access_key_id
      aws_secret_access_key = var.aws_secret_access_key
      aws_default_region = var.aws_default_region
      aws_s3_bucket = aws_s3_bucket.imagebin.id
      aws_s3_website = var.aws_s3_website
      flask_secret_key = random_string.flask.id
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "container_unit" {
  path = "${var.home}/.config/containers/systemd/imagebin.container"
  content {
    content = templatefile("${path.module}/templates/imagebin.container", {
      home = var.home
      publish_host = var.publish_host
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "service_unit" {
  path = "${var.home}/.config/systemd/user/imagebin-purge.service"
  content {
    content = templatefile("${path.module}/templates/imagebin-purge.service", {
      environmentFile = "${var.home}/environment"
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "timer_unit" {
  path = "${var.home}/.config/systemd/user/imagebin-purge.timer"
  content {
    content = file("${path.module}/templates/imagebin-purge.timer")
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_link" "timer_unit" {
  path = "${var.home}/.config/systemd/user/timers.target.wants/imagebin-purge.timer"
  target = data.ignition_file.timer_unit.path
  hard = false
  uid = var.uid
  gid = var.gid
}

data "ignition_link" "purge_timer" {
  path = "${var.home}/.config/systemd/user/timers.target.wants/imagebin-purge.timer"
  target = "${var.home}/.config/systemd/user/imagebin-purge.timer"
  hard = false
}

data "ignition_config" "config" {
  directories = [
    data.ignition_directory.config.rendered,
  ]

  files = [
    data.ignition_file.environment.rendered,
    data.ignition_file.container_unit.rendered,
    data.ignition_file.service_unit.rendered,
    data.ignition_file.timer_unit.rendered,
  ]

  links = [
    data.ignition_link.purge_timer.rendered
  ]
}
